const express = require('express');
const router = express.Router();

//roles import
const { rolPost } = require("../Controllers/roleController.js");
//users import
const { userPost, userLogin } = require("../Controllers/userController.js");
//categories import
const { categoryPost, categoryGet, categoryDelete, categoryPatch } = require("../Controllers/categoryController");
//newsSouces imports
const newsSourceController = require('../Controllers/newsSourceController');

//news imports 
const { getNewssources, getNews,getNewsForCategory,deleteByCategory } = require('../Controllers/newsController');

//roles router
router.post('/roles', rolPost);

//user router
router.post('/user', userPost);
router.get('/user', userLogin);


//category router
router.post('/category', categoryPost);
router.get('/category', categoryGet);
router.delete('/category', categoryDelete);
router.patch('/category', categoryPatch);


//newsSource router
router.post('/newsSource', newsSourceController.create);
router.get('/newsSource', newsSourceController.get);
router.get('/newsSourceCount', newsSourceController.getCount);
router.patch('/newsSource', newsSourceController.update);
router.delete('/newsSource', newsSourceController.delete);
router.delete('/newsSourceCategory', newsSourceController.deleteByCategory);

//news Router
router.get('/news', getNewssources);
router.get('/newslist', getNews);
router.get('/newsCategory', getNewsForCategory);
router.delete('/newsDeleteCategory', deleteByCategory);


module.exports = router;