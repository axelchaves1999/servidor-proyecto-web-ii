const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var roles = mongoose.model('roles');

//user model
const user = new Schema({
    firstName: String,
    lastName: String,
    email: String,
    password: String,
    rol_id: { type: Schema.ObjectId, ref: "roles" , default : "60da5373cf01201a8c14f5ed"}
});

module.exports = mongoose.model('user', user);