const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//course model
const course = new Schema({
    name: String,
    code: String,
    carrer: String,
    credits: Number
});

module.exports = mongoose.model('courses',course);