const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const User = require("../Models/userModel");


//category models
const category = new Schema({
    name: String,
    user_id: { type: Schema.ObjectId, ref: User, default: "60d7ace853e77c50605ca5f5" }
});

module.exports = mongoose.model('categories', category);