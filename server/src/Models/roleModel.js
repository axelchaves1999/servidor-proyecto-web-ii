const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//rol model
const rol = new Schema({
    name: String
});

module.exports = mongoose.model('roles', rol);