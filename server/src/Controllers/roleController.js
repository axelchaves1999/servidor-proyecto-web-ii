const Rol = require("../Models/roleModel");



/**
 * this method creates an rol 
 */
const rolPost = async (req, res) => {
    var rol = new Rol();

    rol.name = req.body.name;


    if (rol) {
        await rol.save(function(err) {
            if (err) {
                res.status(422);
                console.log('error while saving the rol', err)
                res.json({
                    error: 'There was an error saving the rol'
                });
            }
            res.status(201); //CREATED
            res.header({
                'location': `http://localhost:3000/api/rol/?id=${rol.id}`
            });
            res.json(rol);
        });
    } else {
        res.status(422);
        console.log('error while saving the rol')
        res.json({
            error: 'No valid data provided for rol'
        });
    }

};



module.exports = { rolPost };