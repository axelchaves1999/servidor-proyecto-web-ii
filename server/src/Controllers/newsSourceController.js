const NewsSourcesModel = require('../Models/newsSources');
const NewsModel = require('../Models/newsModel');
const Category = require('../Models/categoryModel');
const { readRSS } = require('../Controllers/newsController');


class NewsSource {
/**
 * this method creates a new Source 
 */
    async create(req, res) {
        const newsSource = new NewsSourcesModel(req.body);

        if (newsSource) {
            try {
                await newsSource.save(function (err) {
                    if (err) {
                        res.status(422);
                        console.log('error while saving the newsSource', err)
                        res.json({
                            error: 'There was an error saving the newsSource'
                        });
                    }
                    res.status(201); //CREATED
                    res.header({
                        'location': `http://localhost:3000/newsSources?id=${newsSource.id}`
                    });
                    res.json(newsSource);
                    readRSS(newsSource, res);
                })

            } catch (e) {
                res.status(422);
                console.log('error while saving the newsSource', e.error)
                res.json({
                    error: 'There was an error saving the newsSource'
                });
            }
        } else {
            res.status(422);
            console.log('error while saving the category')
            res.json({
                error: 'No valid data provided for newsSource'
            });

        }
    }
 /**
  * This method return the list of news Sources by user id 
  */
    async get(req, res) {

        if (req.query && req.query.user_id) {

            try {
                await NewsSourcesModel.find({ 'user_id': req.query.user_id }, function (err, newsSources) {
                    if (err) {
                        res.status(422);
                        res.json({ "error": err });
                    }
                    Category.populate(newsSources, { path: 'category_id' }, function (err, newsSources) {
                        res.status(200); // OK
                        res.json(newsSources);
                    })
                })
            } catch (e) {
                res.status(422);
                console.log('error while get newsSource', e.error)
                res.json({
                    error: 'There was an error in the newsSource'
                });
            }
        } else {
            try {
                await NewsSourcesModel.find(function (err, newsSource) {
                    if (err) {
                        res.status(422);
                        console.log('error while get newsSource', err)
                        res.json({
                            error: 'There was an error in the newsSource'
                        });
                    } Category.populate(newsSource, { path: 'category_id' }, function (err, newsSource) {
                        res.status(200); // OK
                        res.json(newsSource);
                    })
                })
            } catch (e) {
                res.status(422);
                console.log('error while get newsSource', e.error)
                res.json({
                    error: 'There was an error in the newsSource'
                });
            }
        }

    }


    /**
     * This method return the count of news sources by user id
     */
    async getCount(req, res) {

        if (req.query && req.query.user_id) {

            try {
                await NewsSourcesModel.count({ 'user_id': req.query.user_id }, function (err, newsSources) {
                    if (err) {
                        res.status(422);
                        res.json({ "error": err });
                    }

                    res.status(200); // OK
                    res.json(newsSources);

                })
            } catch (e) {
                res.status(422);
                console.log('error while get newsSource', e.error)
                res.json({
                    error: 'There was an error in the newsSource'
                });
            }
        }

    }


    /**
     * this method updates a newsSources
     */
    async update(req, res) {

        if (req.query && req.query.id) {
            try {
                await NewsSourcesModel.findById(req.query.id, async function (err, newsSource) {
                    if (err) {
                        res.status(404);
                        console.log('error while queryting the newsSource', err)
                        res.json({ error: "NewsSource doesnt exist" })
                    }
                    newsSource.url = req.body.url ? req.body.url : newsSource.url;
                    newsSource.name = req.body.name ? req.body.name : newsSource.name;
                    newsSource.category_id = req.body.category_id ? req.body.category_id : newsSource.category_id;
                    newsSource.user_id = req.body.user_id ? req.body.user_id : newsSource.user_id;

                    await newsSource.save(function (err) {
                        if (err) {
                            res.status(422);
                            console.log('error while saving the newsSource', err)
                            res.json({
                                error: 'There was an error saving the newsSource'
                            });
                        }
                        res.status(200); // OK
                        res.json(newsSource);
                    })
                })
            } catch (e) {
                res.status(422);
                console.log('error while update the newsSource', e.error)
                res.json({
                    error: 'There was an error update the newsSource'
                });
            }
        }
    }
/**
 * Yhis method removes de news sources by id
 */
    async delete(req, res) {
        if (req.query && req.query.id) {
            try {
                await NewsSourcesModel.findById(req.query.id, async function (err, newsSource) {
                    if (err) {
                        res.status(500);
                        console.log('error while queryting the newsSource', err)
                        res.json({ error: "NewsSource doesnt exist" })
                    }

                    if (newsSource) {
                        await NewsModel.remove({ 'newsSource_id': newsSource._id });
                        await newsSource.remove(function (err) {
                            if (err) {
                                res.status(500).json({ message: "There was an error deleting the newsSource" });
                            }
                            res.status(204).json({});
                        })


                    } else {
                        res.status(404);
                        console.log('error while queryting the category', err)
                        res.json({ error: "NewsSource doesnt exist" })
                    }
                })
            } catch (e) {
                res.status(500);
                console.log('error while queryting the newsSource', e.error)
                res.json({ error: "NewsSource error" })
            }
        }
    }
/**
 * Yhis method removes de news sources by category id
 */
    async deleteByCategory(req, res) {
        if (req.query && req.query.id) {
            try {
                await NewsSourcesModel.deleteOne({ "category_id": req.query.id }, function (err) {
                    if (err) {
                        res.status(500).json({ message: "There was an error deleting the newsSource" });
                    }
                    res.status(204).json({});
                });
            } catch (e) {
                res.status(500);
                console.log('error while queryting the newsSource', e.error)
                res.json({ error: "NewsSource error" })
            }
        }
    }

}

module.exports = new NewsSource();