const Category = require("../Models/categoryModel");


/**
 *This method creates a new Category. 
 */
const categoryPost = async(req, res) => {
    var category = new Category(req.body);

    if (category) {
        await category.save(function(err) {
            if (err) {
                res.status(422);
                console.log('error while saving the category', err)
                res.json({
                    error: 'There was an error saving the category'
                });
            }
            res.status(201); //CREATED
            res.header({
                'location': `http://localhost:3000/category?id=${category.id}`
            });
            res.json(category);
        });
    } else {
        res.status(422);
        console.log('error while saving the category')
        res.json({
            error: 'No valid data provided for category'
        });
    }

};


/**
 * This method return thr list of categories. 
 */
const categoryGet = async(req, res) => {
    // if an specific course is required
    if (req.query && req.query.id) {
        await Category.findById(req.query.id, function(err, category) {
            if (err) {
                res.status(404);
                console.log('error while queryting the Category', err)
                res.json({ error: "Category doesnt exist" })
            }
            res.json(category);
        });
    } else {
        // get all courses
        await Category.find(function(err, categories) {
            if (err) {
                res.status(422);
                res.json({ "error": err });
            }
            res.json(categories);
        });

    }

};
/**
 * This method removes the category by id
 */
const categoryDelete = async(req, res) => {
    // if an specific category is required
    if (req.query && req.query.id) {
        await Category.findById(req.query.id, function(err, category) {
            if (err) {
                res.status(500);
                console.log('error while queryting the category', err)
                res.json({ error: "category doesnt exist" })
            }
            //if the category exists
            if (category) {
                category.remove(function(err) {
                    if (err) {
                        res.status(500).json({ message: "There was an error deleting the category" });
                    }
                    res.status(204).json({});
                })
            } else {
                res.status(404);
                console.log('error while queryting the category', err)
                res.json({ error: "category doesnt exist" })
            }
        });
    } else {
        res.status(404).json({ error: "You must provide a category ID" });
    }
};

/**
 * This method updates the data of a category. 
 */
const categoryPatch = async(req, res) => {
    // get course by id
    if (req.query && req.query.id) {
        await Category.findById(req.query.id, async function(err, category) {
            if (err) {
                res.status(404);
                console.log('error while queryting the category', err)
                res.json({ error: "Category doesnt exist" })
            }

            // update the category object (patch)

            category.name = req.body.name ? req.body.name : category.name;

            await category.save(function(err) {
                if (err) {
                    res.status(422);
                    console.log('error while saving the category', err)
                    res.json({
                        error: 'There was an error saving the category'
                    });
                }
                res.status(200); // OK
                res.json(category);
            });
        });
    } else {
        res.status(404);
        res.json({ error: "Category doesnt exist" })
    }
};


module.exports = { categoryPost, categoryGet, categoryDelete, categoryPatch };