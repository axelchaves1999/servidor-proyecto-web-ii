const newsModel = require("../Models/newsModel");

const newsSourceController = require('../Controllers/newsSourceController');
const url = 'http://localhost:3000/newsSource';
const fetch = require("node-fetch");

let Parser = require('rss-parser');

var DomParser = require('dom-parser');

/**
 *This method reads the news sources and then return the url od news sources 
 */
const getNewssources = (req) => {
    return fetch(url)
        .then(response => response.json())
        .then(data => {
            for (let item of data) {

                readRSS(item)


            }
        });
}



/**
 *This method read the rss with the url of newsSource. 
 */
const readRSS = async(newsSource) => {
    let parser = new Parser();
    let feed = await parser.parseURL(newsSource.url);

    feed.items.forEach(item => {
        // res.send(item + )
        newsPost(newsSource, item)
            // console.log(item)
    });
}


/**
 *This method creates a new news 
 */
const newsPost = async(newsSource, item) => {
    //removeCollection();
    var data = {
        "title": item.title,
        "content": item.content,
        "link": item.link,
        "pubDate": Date.parse(item.pubDate),
        "user_id": newsSource.user_id,
        "category_id": newsSource.category_id,
        "newsSource_id": newsSource._id



    }
    var news = new newsModel(data);
    await news.save();

};


/**
 *This method return the all list of news 
 */
const getNews = async(req, res) => {
    // if a specific id is required for news
    if (req.query && req.query.id) {
        await newsModel.find({ 'user_id': req.query.id }, function(err, news) {
            if (err) {
                res.status(404);
                console.log('error while queryting the news', err)
                res.json({ error: "News doesnt exist" })
            }
            res.json(news);
        }).sort({ _id: -1 }).limit(50);
    } else {
        res.status(404);
        res.json({ error: 'error' })

    }
}
/**
 *This method  return the news by id category
 */
const getNewsForCategory = async(req, res) => {
    // if a specific id is required for news
    if (req.query && req.query.category_id) {
        await newsModel.find({ "category_id": req.query.category_id,"user_id": req.query.user_id }, function(err, news) {
            if (err) {
                res.status(404);
                console.log('error while queryting the news', err)
                res.json({ error: "News doesnt exist" })
            }
            res.json(news);
        }).sort({ _id: -1 }).limit(50);
    } else {
        res.status(404);
        res.json({ error: 'error' })
    }

}

/**
 *This method removes the news by  id category 
 */
const deleteByCategory =async (req, res) => {
    if (req.query && req.query.id) {
        try {
            await newsModel.deleteOne({ "category_id": req.query.id }, function (err) {
                if (err) {
                    res.status(500).json({ message: "There was an error deleting the news" });
                }
                res.status(204).json({});
            });
        } catch (e) {
            res.status(500);
            console.log('error while queryting the news', e.error)
            res.json({ error: "News error" })
        }
    }
}

module.exports = { readRSS, getNewssources, getNews, getNewsForCategory ,deleteByCategory};