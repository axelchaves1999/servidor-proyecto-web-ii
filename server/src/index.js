const express = require('express');
const mongoose = require("mongoose");
const morgan = require('morgan');
const app = express();

//Conection with the database
mongoose.connect('mongodb://localhost/mynewscover')
    .then(db => console.log('Db  connected'))
    .catch(err => console.log(err))

    //routes 
const indexRoutes = require('./routes/index');

const cors = require("cors");
app.use(cors({
    domains: '*',
    methods: "*"
}));

//middlewares
app.use(morgan('dev'));
//body -parser
const bodyParser = require("body-parser");
app.use(bodyParser.json());

//routes
app.use('', indexRoutes)

app.listen(3000, () => console.log('App listen on port 3000'));